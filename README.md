
![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

This is an introductory project to the Babylon gaming engine meant to be used on a desktop computer. It includes a humanoid figure which walks in the direction of the user's choosing over flat and mountainous landscapes. If you click on the "Background" button in the top left-hand corner you will see a drop-down menu which allows you to change the background between two different displays. This demonstration also includes an autonomous vehicle which simply follows a warped circle around the scene.

## Demo in action!!
> https://turingtester.gitlab.io/babylonjs-intro/demo/

## Contact
> https://turingtester.gitlab.io/babylonjs-intro/demo/contact.html

Demo was pieced together via the following three components...

## Humanoid figure on mountainous terrain
> https://ssatguru.github.io/BabylonJS-CharacterController-Samples/demo/

## Autonomous car
> https://www.babylonjs-playground.com/#1YD970%2314

## Dropdown menu
> https://www.babylonjs-playground.com/#H10NI4#5
